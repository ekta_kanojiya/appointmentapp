import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addeditappointment',
  templateUrl: './appointment-addedit.component.html',
  styleUrls: ['./appointment-addedit.component.css']
})
export class AddEditappointmentComponent implements OnInit {
  addAppointForm: FormGroup;
  appointmentId : number;
  constructor(private formBuilder: FormBuilder, private router: Router,private actRoute : ActivatedRoute ) {
    this.addAppointForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(100), Validators.required])],
      phone: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.actRoute.params.subscribe(params => {
        this.appointmentId = params['id'];
    });
    if(this.appointmentId){
         this.getAppointmentData();
    }
    else{
      console.log('add mode');
    }
  }

  addEditAppointment() {
    var index = this.actRoute.snapshot.queryParams["index"]
    this.router.navigate(['appointment/timesloats'],{queryParams : {id:1,index:index}});

  }

 //set dummy data in edit mode
  getAppointmentData(){
    this.addAppointForm.controls.firstName.setValue('Test user');
    this.addAppointForm.controls.lastName.setValue('test user');
    this.addAppointForm.controls.phone.setValue('9349348121');
  }
}
