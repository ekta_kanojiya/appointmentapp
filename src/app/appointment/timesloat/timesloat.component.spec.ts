import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesloatComponent } from './timesloat.component';

describe('TimesloatComponent', () => {
  let component: TimesloatComponent;
  let fixture: ComponentFixture<TimesloatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesloatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesloatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
