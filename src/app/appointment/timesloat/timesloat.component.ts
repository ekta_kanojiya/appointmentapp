import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { mockData } from './mocktimesloats';

@Component({
  selector: 'app-timesloat',
  templateUrl: './timesloat.component.html',
  styleUrls: ['./timesloat.component.css']
})
export class TimesloatComponent implements OnInit {
  timesloats: Array<any>;
  constructor(private router: Router, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.timesloats = mockData;
    if (this.actRoute.snapshot.queryParams["id"]) {
      var index = this.actRoute.snapshot.queryParams["index"]//mock data so need to do in this way
      this.timesloats[index].isBooked = true;
    }
  }

  redirectToDetail(isBooked: boolean, index: number) {
    if (!isBooked) {//redirect add appointment
      this.router.navigate(['appointment/book'], { queryParams: { index: index } });
    }
    else {//redirect edit appointment
      this.router.navigate(['appointment/book/1/edit'], { queryParams: { index: index } });
    }

  }

}
