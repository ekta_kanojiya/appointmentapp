import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentComponent } from './appointment.component';
import { TimesloatComponent } from './timesloat/timesloat.component';
import { AddEditappointmentComponent } from './appointment-addedit/appointment-addedit.component';

const appointmentRoutes: Routes = [
    {
        path: 'appointment', component: AppointmentComponent,
        children: [
            { path: '', redirectTo: 'timesloats', pathMatch: 'full' },
            { path: 'timesloats', component: TimesloatComponent },
            { path: 'book', component: AddEditappointmentComponent },
            { path: 'book/:id/edit', component: AddEditappointmentComponent },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appointmentRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppointmentRoutingModule { }