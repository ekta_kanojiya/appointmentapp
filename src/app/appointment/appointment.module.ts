import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppointmentComponent } from './appointment.component';
import { AppointmentRoutingModule } from './appointment.routes';
import { TimesloatComponent } from './timesloat/timesloat.component';
import { AddEditappointmentComponent } from './appointment-addedit/appointment-addedit.component';


@NgModule({
    declarations: [
        AppointmentComponent,
        TimesloatComponent,
        AddEditappointmentComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        AppointmentRoutingModule
    ],
    providers: []
})
export class AppointmentModule { }
